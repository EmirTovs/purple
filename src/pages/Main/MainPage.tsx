import React, { useState, useRef } from 'react';
import classNames from "classnames/bind";
import Grid from '@material-ui/core/Grid';
import styles from './MainPage.module.scss';
import { Link } from 'react-router-dom';


const cx = classNames.bind(styles);

const MainPage = () => {
    return (
        <Grid className={cx('container')} container>
          <Link to="/home">Hello</Link>
          <Link to="/second-page">Second Page</Link>
           MainPage
        </Grid>
    );
};

export {MainPage};
