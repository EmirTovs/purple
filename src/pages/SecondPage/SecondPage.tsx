import React from 'react';
import styles from './SecondPage.module.scss';
import classNames from "classnames/bind";
import Grid from '@material-ui/core/Grid';
import {Link, useHistory} from 'react-router-dom';

const cx = classNames.bind(styles);

const SecondPage = () => {
    const history = useHistory();
    
    return (
        <>
          <Link to="/home">Hello</Link>
          <Link to="/second-page">Second Page</Link>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                className={cx(styles.container)}
            >
              Second Page
            </Grid>
        </>
    );
};

export {SecondPage};
